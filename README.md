# Unicrawl

Sistema de gamificación de Tareas en red.
Completa tareas, vence enemigos, gana experiencia, y supera el puntaje de los demás.
O, si lo prefieres, publica tareas para otros y observa quienes las completan a tiempo.
Unicrawl te da las herramientas para embarcarte en cualquier aventura, o, crear la tuya propia.
**La desición es tuya.**

## Compilación y ejecución

Unicrawl tiene dos componentes: un cliente y un servidor, ambos presentes en este repositorio.
Para compilarlo necesitarás [mono](https://mono-project.com), la implementación código abierto de .NET.

1. Clonar el repositorio:

```
$ git clone https://gitlab.com/fergonzr/unicrawl.git
$ cd unicrawl
```

2. Dentro de él, compilarlo, bien sea usando `make`, o `mono-csc` directamente:

```
$ make  # compilará tanto el cliente como el servidor
$ mono-csc -r:System.Net.Http.dll -r:Newtonsoft.Json.dll -r:Mono.Data.Sqlite.dll core.cs client/*.cs -out:uniclient.exe # cliente
$ mono-csc -r:System.Net.Http.dll -r:Newtonsoft.Json.dll -r:Mono.Data.Sqlite.dll core.cs server/*.cs -out:uniserver.exe # servidor
```

Mediante ambas opciones, el cliente y servidor quedarán compilados como `uniclient.exe` y `uniserver.exe`, respectivamente.
Cópialos en la carpeta que necesites y ejecútalos con:

```
$ mono uniclient.exe # cliente
$ mono uniserver.exe # servidor
```

## Instrucciones de uso
### Servidor

La base de datos SQLite que usa el servidor se crea automáticamente en la carpeta en que corras el ejecutable, idealmente, no la borres.
El servidor escucha en todas las direcciones disponibles en el puerto `8080/tcp`, verifica que tu firewall admita tráfico a través de él.

### Cliente

Lo primero que te va a preguntar el cliente es la dirección del servidor, la cual debes especificar de manera completa, de la siguiente forma:

```
http://srvunicrawl.com:8080
```

Por supuesto, reemplaza `srvunicrawl.com` por el nombre ó ip de tu servidor.

La configuración de perfil y conexión se guarda en el archivo `uniclient.json` en la carpeta donde ejecutas el cliente, idealmente, tampoco la borres.

### Keybinds:
#### Tablas
- `K` ó `<FlechaArriba>` para ir a la entrada de arriba.
- `J` ó `<FlechaAbajo>` para ir a la entrada de abajo.
- `<Enter>` para seleccionar la entrada actual.
- `Q` para salir sin seleccionar nada.

Ir hacia arriba estando en la primera entrada te llevará a seleccionar los filtros de la tabla:

#### Filtros
- `H` ó `<FlechaIzquierda>` para ir al campo de la izquierda.
- `L` ó `<FlechaDerecha>` para ir al campo de la derecha.
- `J` ó `<FlechaAbajo>` para ordenar las entradas en orden descendente.
- `K` ó `<FlechaArriba>` para ordenar las entradas en orden ascendente.
- `F` para aplicar un filtro de selección a las entradas.
- `<Enter>` para aplicar los filtros deseados y volver a la tabla.

Si la tabla no tiene entradas que se ajusten a los filtros que seleccionaste, saldrás de ella automáticamente.

#### Menús
- `K` ó `<FlechaArriba>` para ir a la opción de arriba.
- `J` ó `<FlechaAbajo>` para ir a la opción de abajo.
- `<Enter>` para seleccionar la opción actual.

## To-Do:
- [x] Traducir al Español
- [ ] Brindar una manera de especificar la dirección, puerto de escucha y ruta de la base de datos al servidor.
- [ ] Guardar la configuración de conexión del cliente en una carpeta estándar (`~/.config/unicrawl`?).
- [ ] Mejorar logs del servidor
