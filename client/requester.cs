using System;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

abstract class JsonRequestable {}

class CharacterRequest : JsonRequestable {
	public string character;
	public string asciiArt;
	public CharacterRequest(Character c)
	{
		this.character = c.Name;
		this.asciiArt = c.AsciiArt;
	}
}

class RequestErrorException : Exception {
	public string Error {get;}
	[JsonConstructor]
	public RequestErrorException(string error)
	{
		this.Error = error;
	}
}

class NonRequestableException : Exception {
	public Type AttemptedType {get;}
	public NonRequestableException(Type t)
	{
		this.AttemptedType = t;
	}
}

class UserUpdateEventArgs : EventArgs {
	public int ExperienceGain {get;}
	public int LevelGain {get;}
	public Assignment[] NewAssignments {get;}
	public UserUpdateEventArgs(User old, User updated)
	{
		if (old != null && updated != null &&
			old.Assignments != null && updated.Assignments != null){
			this.ExperienceGain = updated.Xp - old.Xp;
			this.LevelGain = updated.Level - old.Level;
			this.NewAssignments = Enumerable.Except(
				updated.Assignments,
				old.Assignments,
				new AssignmentComparer()
			).ToArray();
		} else
			NewAssignments = new Assignment[0];
	}
}

class RequesterConfig {
	public string serverUrl;
	public AuthUser user;
}

class Requester {
	static readonly HttpClient client = new HttpClient();
	public AuthUser LoggedUser {get; private set;}
	public User[] Peers {get; private set;}
	public Stack<Task> ScheduledTasks {get; private set;}
	public delegate void UserUpdateEventHandler(object sender, UserUpdateEventArgs a);
	public event UserUpdateEventHandler UserUpdate;
	public event EventHandler PeerInfoUpdated;
	private const int updateDelay = 4000; // 30 second delay
	public string serverUrl {get;}

	public bool BackgroundSyncEnabled {
		get => this.backgroundSyncTask != null
		    && this.backgroundSyncTask.Status == TaskStatus.Running;
	set
		{
			if (value){
				syncCancelSource = new CancellationTokenSource();
				this.backgroundSyncTask = Task.Run( () => {
						Task.WaitAll(
							BackgroundSyncUserInfo(syncCancelSource.Token),
							BackgroundSyncPeerInfo(syncCancelSource.Token)
						);
				});
			}
			else {
				this.syncCancelSource.Cancel();
			}
		}
	}
	public Task backgroundSyncTask;
	private CancellationTokenSource syncCancelSource;

	private Object mkRequestable(Object obj)
	{
		return obj.GetType().Name switch {
			"Character" => new {
				character = ((Character) obj).Name,
				asciiArt  = ((Character) obj).AsciiArt
			},
			"Project" => new {
				projectname  = ((Project) obj).Name,
				projectopen  = ((Project) obj).Opening,
				name = LoggedUser.Name,
				token = LoggedUser.Token
			},
			"Assignment" => new {
				title = ((Assignment) obj).Title,
				enemy = ((Assignment) obj).Enemy.Name,
				description = ((Assignment) obj).Description,
				rank = ((Assignment) obj).Rank > 0 ? ((Assignment) obj).Rank : 0,
				dueDate = ((Assignment) obj).DueDate,
				name = LoggedUser.Name,
				token = LoggedUser.Token
			},
			_ => throw new NonRequestableException(obj.GetType())
		};
	}
	private StringContent mkStrJsonContent(Object obj)
		=> new StringContent(
			JsonConvert.SerializeObject(obj),
			new UTF8Encoding(),
			"application/json"
		);

	public async Task RetrieveLoggedUser(string name, string token)
	{
		AuthUser u = new AuthUser(await this.LookupUser(name), token);
		if ( UserUpdate != null )
			UserUpdate(this, new UserUpdateEventArgs(this.LoggedUser, u));
		this.LoggedUser = u;
	}
	public async Task RetrieveLoggedUser()
	{
		AuthUser u = new AuthUser(await this.LookupUser(LoggedUser.Name), LoggedUser.Token);
		if ( UserUpdate != null )
			UserUpdate(this, new UserUpdateEventArgs(this.LoggedUser, u));
		this.LoggedUser = u;
	}
	public async Task<User> LookupUser(string name)
	{
		string response = await client.GetStringAsync(
			serverUrl + "/user?name=" + HttpUtility.UrlEncode(name));
		if ( JObject.Parse(response)["error"] != null )
			throw JsonConvert.DeserializeObject<RequestErrorException>(response);
		return JsonConvert.DeserializeObject<User>(response);
	}
	public int SchedulePeerLookup(string name)
	{
		try {
			if ( this.Peers.Single(u => u.Name == name).Assignments != null )
				return -1;
		} catch {
			return -1;
		}
		this.ScheduledTasks.Push(Task<User>.Run(async () =>
			this.Peers[new List<User>(this.Peers).FindIndex(u => u.Name == name)]
				= await LookupUser(name)
		));
		return this.ScheduledTasks.Count;
	}
	public async Task<AuthUser> CreateUser(string name, string character)
	{
			// return LoggedUser = JsonConvert.DeserializeObject<AuthUser>(
			string response = await (await client.PostAsync(serverUrl
					+ "/newUser?name=" + HttpUtility.UrlEncode(name)
					+ "&" + "character=" + HttpUtility.UrlEncode(character),
				new StringContent("")))
				.Content.ReadAsStringAsync();
			if ( JObject.Parse(response)["error"] != null )
				throw JsonConvert.DeserializeObject<RequestErrorException>(response);
			return LoggedUser = JsonConvert.DeserializeObject<AuthUser>(response);
	}
	public static async Task<Requester> CreateFirstTimeRequester(string serverUrl, string username, string character)
	{
		Requester req = new Requester(serverUrl);
		await req.CreateUser(username, character);
		return req;
	}
	public static async Task<Requester> CreateRequester(string serverUrl, string username, string token)
	{
		Requester req = new Requester(serverUrl);
		await req.RetrieveLoggedUser(username, token);
		return req;
	}
	public static async Task<Requester> CreateRequester(string serverUrl, AuthUser u)
	{
		Requester req = new Requester(serverUrl, u);
		await req.RetrieveLoggedUser(req.LoggedUser.Name, req.LoggedUser.Token);
		return req;
	}
	public Requester(string serverUrl)
	{
		this.serverUrl = serverUrl;
		this.ScheduledTasks = new Stack<Task>();
	}
	public Requester(string serverUrl, AuthUser u)
	{
		this.serverUrl = serverUrl;
		this.LoggedUser = u;
		this.ScheduledTasks = new Stack<Task>();
		this.ScheduledTasks.Push(RetrieveLoggedUser());
	}
	public Requester(RequesterConfig config)
	{
		this.serverUrl = config.serverUrl;
		this.LoggedUser = config.user;
		this.ScheduledTasks = new Stack<Task>();
		this.ScheduledTasks.Push(RetrieveLoggedUser());
	}
	public async Task BackgroundSyncUserInfo(CancellationToken t)
	{
		try {
			while (!t.IsCancellationRequested) {
				await RetrieveLoggedUser(LoggedUser.Name, LoggedUser.Token);
				await Task.Delay(updateDelay);
			}
		} catch (HttpRequestException) {
			Console.WriteLine("Network eror, plase try again later.");
			this.BackgroundSyncEnabled = false;
		}
	}
	public async Task<User[]> RetrievePeerInfo(){
		User[] updPeers = JsonConvert.DeserializeObject<User[]>(
			await client.GetStringAsync(serverUrl + "/friends?name=" +
				HttpUtility.UrlEncode(LoggedUser.Name)
		));
		return this.Peers = updPeers;
	}
	public async Task BackgroundSyncPeerInfo(CancellationToken t)
	{
		try {
			while (!t.IsCancellationRequested) {
				await RetrievePeerInfo();
				if (PeerInfoUpdated != null)
					PeerInfoUpdated(this, EventArgs.Empty);
				await Task.Delay(updateDelay);
			}
		} catch (HttpRequestException) {
			Console.WriteLine("Network eror, plase try again later.");
			this.BackgroundSyncEnabled = false;
		}
	}

	public async Task<T[]> GET_Objects<T>()
	{
		if ( !(typeof(T) == typeof(Project) || typeof(T) == typeof(Character)) )
			return null;
		// what a hack, I know, but it definitely works :)
		return JsonConvert.DeserializeObject<T[]>(
				await client.GetStringAsync(serverUrl + "/" + typeof(T).Name.ToLower() + "s"
			));
	}
	public async Task<Project[]> GET_Projects(string user, bool isSubscribed)
		=> JsonConvert.DeserializeObject<Project[]>(
				await client.GetStringAsync(serverUrl + "/projects?user="
				+ HttpUtility.UrlEncode(user)
				+ (isSubscribed ? "&isSubscribed=1" : "")
				));
	public async Task<ProjectStats> GET_projectInfo(string project)
	{
		return JsonConvert.DeserializeObject<ProjectStats>(
			await client.GetStringAsync(
				serverUrl + "/projectInfo?project=" + 
				HttpUtility.UrlEncode(project) 
			)
		);
	}

	public async Task<string> POST_newObject<T>(T obj)
	{
		using HttpResponseMessage response =
			await client.PostAsync(serverUrl + "/new" + typeof(T).Name, mkStrJsonContent(mkRequestable(obj)));
		if ( !response.IsSuccessStatusCode ){
			Console.WriteLine(await response.Content.ReadAsStringAsync());
			throw JsonConvert.DeserializeObject<RequestErrorException>(
				await response.Content.ReadAsStringAsync()
			);
		}
		return await response.Content.ReadAsStringAsync();
	}

	public async Task POST_subscribeProj(string projname)
	{
		using HttpResponseMessage response = await client.PostAsync(
			serverUrl + "/subscribeProj", mkStrJsonContent(
			new {
				projectname = projname,
				name = LoggedUser.Name,
				token = LoggedUser.Token
			})
		);
		if ( !response.IsSuccessStatusCode )
			throw JsonConvert.DeserializeObject<RequestErrorException>(
				await response.Content.ReadAsStringAsync()
			);
	}
	public async Task POST_completeAssignment(string uid)
	{
		using HttpResponseMessage response = await client.PostAsync(
			serverUrl + "/completeAssignment", mkStrJsonContent(
			new {
				uid = uid,
				name = LoggedUser.Name,
				token = LoggedUser.Token
			})
		);
		if ( !response.IsSuccessStatusCode )
			throw JsonConvert.DeserializeObject<RequestErrorException>(
				await response.Content.ReadAsStringAsync()
			);
	}

	public void WaitForUserUpdate()
	{
		if ( !this.BackgroundSyncEnabled )
			return;
		using ManualResetEventSlim waiter = new ManualResetEventSlim(false);
		UserUpdateEventHandler releaseWaiter = (object sender, UserUpdateEventArgs e)
			=> waiter.Set();
		this.UserUpdate += releaseWaiter;
		waiter.Wait();
		this.UserUpdate -= releaseWaiter;
	}
	public RequesterConfig ObtainConfig() => new RequesterConfig {
		serverUrl = this.serverUrl,
		user = this.LoggedUser
	};
}
