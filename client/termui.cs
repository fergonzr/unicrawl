/*
 * Estas son algunas helper functions para el manejo de menús y prompts,
 * utilizando secuencias de escape ANSI. No estoy seguro, de primera
 * mano, si corre 100% bien en la terminal de Windows. Para referencia:
 * https://en.wikipedia.org/wiki/ANSI_escape_code
 */
using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

class Termui {
	public delegate int EventOptionDisplayer(int left, int top, string option);
	public static event EventOptionDisplayer OptionDisplayer;
	public static event EventHandler ScreenCleared;
	public const byte comfortableHeight = 40;
	public const byte notifyWidth = 40;
	public static (int, int) lastCursorPos;
	public static string banner = "";
	public static void PushCursorPos()
	{
		lastCursorPos = (Console.CursorLeft, Console.CursorTop);
	}
	public static void PopCursorPos()
	{
		Console.SetCursorPosition(lastCursorPos.Item1, lastCursorPos.Item2);
		lastCursorPos = (0,0);
	}
	public static string Colorize(string s, int n)
		=> "\u001B[" + (37 - (n % 7)) + "m" + s + "\u001B[0m";
	public static string MultiLinePrompt(string prompt, bool shouldPrompt)
	{
		if (shouldPrompt)
			Console.WriteLine("Please supply {0} ('.' to end).", prompt);
		string s = Console.ReadLine();
		if (s.Equals("."))
			return "";
		return s + "\n" + MultiLinePrompt("", false);
	}
	public static string SinglePrompt(string prompt)
		=> MultiPrompt(new string[]{ prompt }, new bool[]{false})[0];
	public static string[] MultiPrompt(string[] prompts, bool[] shouldHide)
	{
		if ( prompts.Length != shouldHide.Length )
			throw new ArgumentException();
		Console.CursorVisible = true;
		int pos = 0;
		int maxlen = prompts.Max(p => p.Length);
		ConsoleKeyInfo ki;
		string[] inputs = Enumerable.Repeat("", prompts.Length).ToArray();
		// Print each of the options, bolded
		foreach(string prompt in prompts)
			Console.Write(string.Format("\n\u001B[1m{0," + maxlen + ":s}: ", prompt));
		// Go to first prompt
		if (prompts.Length > 1)
			Console.Write("\u001B[" + (prompts.Length - 1) + "A");
		// Reset console
		Console.Write("\u001B[0m");
		
		while (pos < prompts.Length){
			while (((ki = Console.ReadKey(true)).Key != ConsoleKey.Tab
					&& ki.Key != ConsoleKey.Enter) || inputs[pos].Length <= 0){
				// handle backspace key by erasing
				if (ki.Key == ConsoleKey.Backspace && inputs[pos].Length > 0){
					inputs[pos] = inputs[pos][..(inputs[pos].Length-1)];
					Console.Write("\u001B[D");
					Console.Write(" ");
					Console.Write("\u001B[D");
				} else if (!char.IsControl(ki.KeyChar) ) {
					inputs[pos] += ki.KeyChar;
					Console.Write(shouldHide[pos] ? '*' : ki.KeyChar );
				}

			}
			pos++;
			Console.Write("\u001B[E" + "\u001B[" + (maxlen + 2) + "C");
		}
		Console.Write("\n");
		return inputs;
	}
	public static int ClearArea(int left, int top, int right, int rows)
		=> PrintWrapped(left, top, right, rows, true, "");
	// Print s wrapping it to the specified dimensions, limiting it to the specified number of rows
	public static int PrintWrapped(int left, int top, int right, int rows, bool verbatim, string s)
	{
		if (rows <= 0 )
			return 0;
		if ( top >= Console.BufferHeight  )
			Console.CursorLeft = left;
		else
			Console.SetCursorPosition(left, top);
		int sppos = -1;
		int lbpos = s.IndexOf('\n');
		int wrappingPos = -1;
		// Wrap by newline
		if ( lbpos >= 0 && lbpos < right - left )
			wrappingPos = lbpos;
		// Nothing else to print
		else if (right - left > s.Length) {
			Console.Write(s);
			Console.Write(string.Format("{0," + (right - left - s.Length) + ":d}", " "));
			return PrintWrapped(left, top + 1, right, rows - 1, verbatim, "");
		}
		// Wrap by space
		sppos = !verbatim && right - left < s.Length
			? s.LastIndexOf(' ', right - left)
			: -1;
		if ( sppos > 0 && (lbpos < 0 || sppos < lbpos) && sppos < right - left)
			wrappingPos = sppos;
		// Hard-wrap
		else  if ( wrappingPos == -1 )
			wrappingPos = right - left - 1;

		Console.Write(s[..wrappingPos]);
		Console.Write(string.Format("{0," + (right - left - wrappingPos) + ":d}", " "));
		return 1 + PrintWrapped(left, top + 1, right, rows - 1, verbatim, s[(wrappingPos+1)..]);
	}
	public static async void Notification(string s)
	{
			EventHandler notif = (sender,e) =>
				PrintWrapped(Console.BufferWidth - notifyWidth - 2, 1, Console.BufferWidth - 2, 4, false,
				"\u001B[7m" + s + "\u001B[0m");
			PushCursorPos();
			notif(null, EventArgs.Empty);
			PopCursorPos();
			ScreenCleared += notif;
			await Task.Delay(5000);
			ScreenCleared -= notif;
			PushCursorPos();
			ClearArea(Console.BufferWidth - notifyWidth - 2, 1, Console.BufferWidth - 2, 4);
			PopCursorPos();

	}
	public static string FormatRow(int[] lens, string[] row)
		=> string.Join("  ", Enumerable.Range(0,row.Length)
					.Select(i => string.Format("{0,-" + lens[i] + ":s}", row[i]))
				);
	public static string FormatHeader(int pos, int[] lens, string[] headers)
		=> string.Format(string.Join("  ",
			Enumerable.Range(0,headers.Length)
				.Select(i => ((i == pos) ? "\u001B[7m" : "")
					+ ("{" + i + ",-" + lens[i] + ":s}")
					+ ((i == pos) ? "\u001B[0m" : "")))
			, headers
		);
	public static string[] IndicateOrder(int pos, string[] sarr, bool ascending)
	{
		string[] rel = new string[sarr.Length];
		Array.Copy(sarr, rel, sarr.Length);
		int parenpos = sarr[pos].IndexOf('(');
		if (parenpos >= 0 )
			rel[pos] = sarr[pos][..parenpos].ToString() + "("
				+ (ascending ? "v" : "^")
				+ (sarr[pos][parenpos..].Contains("*") ? "*" : "") + ")";
		else
			rel[pos] = sarr[pos] + " (" + (ascending ? "v" : "^") + ")";
		return rel;
	}

	public static string[] IndicateFilter(int pos, string[] sarr)
	{
		string[] rel = new string[sarr.Length];
		Array.Copy(sarr, rel, sarr.Length);
		int parenpos = sarr[pos].IndexOf('(');
		if (parenpos >= 0)
			rel[pos] = sarr[pos][..parenpos]
				+ (sarr[pos][parenpos..].Contains("*") ? sarr[pos][parenpos..]
				: (sarr[pos][parenpos..(parenpos+2)] + "*)"));
		else
			rel[pos] = sarr[pos] + "(*)";
		return rel;
	}

	// Return a function to obtain objects apropriate for ordering
	public static Func<string,IComparable> AppropriateOrderable(string[][] fields, int pos)
	{
		if ( fields.All(row => Client.Successes(() => int.Parse(row[pos]))) )
			return (s) => int.Parse(s);
		if ( fields.All(row => Client.Successes(() => DateTime.Parse(row[pos]))) )
			return (s) => DateTime.Parse(s);
		return (s) => (s);
	}
	public static Func<string[][]>PromptFilter(Func<string[][]> rowGen, int pos)
	{
		Console.Write("\u001B[" + 2 + "A");
		string pattern = SinglePrompt("Filter");
		Console.Write("\u001B[A");
		return () => rowGen().Where(row => row[pos].Contains(pattern)).ToArray();
	}
	public static (string[], Func<string[][]>) ApplyFilters(int pos, int[] lens, string[] headers, Func<string[][]> rowGen)
	{
		Console.Write(FormatHeader(pos-1, lens, headers[1..]));
		Console.Write("\u001B[G");
		ConsoleKeyInfo ki = Console.ReadKey(true);
		Func<string,IComparable> orderConverter = AppropriateOrderable(rowGen(), pos);
		return ki.Key switch {
			ConsoleKey.H or ConsoleKey.LeftArrow 
				=> ApplyFilters(pos + (pos > 1 ? -1 : 0), lens, headers, rowGen ),
			ConsoleKey.L or ConsoleKey.RightArrow
				=> ApplyFilters(pos + ((pos + 1) < headers.Length ?  1 : 0), lens, headers, rowGen ),
			ConsoleKey.K or ConsoleKey.UpArrow
				=> ApplyFilters(pos, lens, IndicateOrder(pos, headers, false),
					(() => rowGen()
					       .ToList()
					       .OrderByDescending(a => orderConverter(a[pos]))
					       .ToArray())),
			ConsoleKey.J or ConsoleKey.DownArrow
				=> ApplyFilters(pos, lens, IndicateOrder(pos, headers, true),
					(() => rowGen()
					       .ToList()
					       .OrderBy(a => orderConverter(a[pos]))
					       .ToArray())),
			ConsoleKey.F =>
				ApplyFilters(pos, lens, IndicateFilter(pos, headers),
					PromptFilter(rowGen, pos)
				),
			ConsoleKey.Enter => (headers, rowGen),
			_ => ApplyFilters(pos, lens, headers, rowGen)
		};
	}
	/* A dynamic, filterable, orderable & colorable table.
	 * The return value is the string on the first position of the
	 * selected row. The first column is considered as identifiers,
	 * so it is hence not displayed.
	 * the function transform is applied to each row individually.
	 */
	public static string ColorTable(int top, int pos, bool shouldSelect,
		string[] headers, Func<string[][]>rowGen, int colorCol)
	=> PrintTable(top, pos, shouldSelect, headers, rowGen,
		   (sarr,s) => {
			   try {
					return Colorize(s,int.Parse(sarr[colorCol]));
				} catch (FormatException) {
					return s;
				}
		   });
	public static string PrintTable(int top, int pos, bool shouldSelect,
		string[] headers, Func<string[][]>rowGen)
		=> PrintTable(top, pos, shouldSelect, headers, rowGen, (sarr, s) => s);
	public static string PrintTable(int top, int pos, bool shouldSelect,
		string[] headers, Func<string[][]> rowGen, Func<string[], string, string>transform)
	{
		Console.Clear();
		Console.WriteLine(banner);
		// Give us one extra line to work with
		Console.WriteLine();
		if ( top < 0 || top >= Console.BufferHeight )
			top = Console.CursorTop + 1;
		Console.CursorTop = top;
		Console.CursorLeft = 0;
		Console.CursorVisible = false;
		string[][] table = new string[rowGen().Length + 1][];
		// No data to show
		if (table.Length <= 1)
			return "";
		table[0] = headers;
		rowGen().CopyTo(table, 1);
		// Obtain the maximum lengths of all fields
		int[] maxlens = table
			.Select(sarr => sarr[1..].Select(s => s.Length))
			.Aggregate((old, cur) => Enumerable.Range(0,old.Count())
				.Select( i => old.ElementAt(i) > cur.ElementAt(i)
					        ? old.ElementAt(i) : cur.ElementAt(i))
			).ToArray();
		
		if ( pos == 0 ){
			(headers, rowGen) = ApplyFilters(1, maxlens, headers, rowGen);
			return PrintTable(top, 1, true,
				headers, rowGen, transform
			);
		}

		// Console.WriteLine(maxlens.Length + " " + table[1].Length);
		// Print each row
		Console.Write(string.Join("\n",
			table.Select(row => transform(row, FormatHeader(-1, maxlens, row[1..])))
		));
		// Go to specified position, highlight it, reset
		if ( pos + 1 < table.Length )
			Console.Write("\u001B[" + (table.Length - pos - 1) + "A");
		Console.Write("\u001B[G\u001B[7m"
			+ transform(table[pos], FormatHeader(-1,maxlens,table[pos][1..]))
			+ "\u001B[0m");
		
		// Move the cursor to the beggining of the table, so we can clean it later.
		Console.Write("\u001B[" + pos + "F");

		if ( !shouldSelect )
			return "";

		if ( OptionDisplayer != null ){
			OptionDisplayer(maxlens.Sum(n => n) + (maxlens.Length - 1) * 2 + 5, top, table[pos][0]);
			Console.Write("\u001B[G");
		}
		if ( ScreenCleared != null ) ScreenCleared(null, EventArgs.Empty);
		
		ConsoleKeyInfo ki = Console.ReadKey(true);
		// Clean table, printing spaces all the way
		Console.Write(string.Join("\n",
			table.Select(row =>
			string.Format("{0," + (maxlens.Sum(n => n) + (maxlens.Length - 1) * 2) + ":s}", "")
			)
		));
		
		// Go to the beggining
		Console.Write("\u001B[" + (table.Length - 1) + "F");

		// Selection
		if ( ki.Key == ConsoleKey.Enter ) {
			Console.CursorVisible = true;
			return table[pos][0];
		// Cancellation
		} else if ( ki.Key == ConsoleKey.Q ){
			Console.CursorVisible = true;
			return "";
		}
		// If the user didn't make the selection, print table again with the adjusted position
		return PrintTable(top, pos + ki.Key switch {
			ConsoleKey.K or ConsoleKey.UpArrow   => (pos > 0) ? -1 : 0,
			ConsoleKey.J or ConsoleKey.DownArrow => (pos + 1 < table.Length) ?  1 : 0,
			_ => 0
		}, true, headers, rowGen, transform);
	}
	public static int Menu(params string[] options)
		=> Menu(0, options);
	public static int Menu(int pos, params string[] options)
	{
		Console.CursorVisible = false;
		ConsoleKeyInfo ki;
		int top = Console.CursorTop;
		int maxlen = options.Max(s => s.Length);

		Console.Clear();
		Console.WriteLine(banner);
		for (int i = 0; i < options.Length; i++)
			Console.Write((i == pos ? "\u001B[7m" : "") + options[i] + "\u001B[0m\u001B[E");

		if ( ScreenCleared != null ) ScreenCleared(null, EventArgs.Empty);

		ki = Console.ReadKey(true);
		if ( ki.Key == ConsoleKey.Enter ){
			Console.CursorVisible = true;
			return pos;
		}
		return Menu(
			ki.Key switch {
				ConsoleKey.UpArrow or ConsoleKey.K => pos > 0 ? pos - 1 : pos,
				ConsoleKey.DownArrow or ConsoleKey.J => pos + 1 < options.Length ? pos + 1 : pos,
				_ => pos
			}, options
		);
	}
}
