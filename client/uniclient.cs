using System;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Diagnostics;
using System.Linq;


class Client {
	public static string configFile = "uniclient.json";
	// INIT
	public static async Task<Requester> ObtainRequester()
	{
		try {
			return new Requester(
				JsonConvert.DeserializeObject<RequesterConfig>(
					File.ReadAllText(configFile)
				)
			);
		} catch (IOException) {
			return await ObtainFirstTimeRequester();
		}
	}
	public static bool Successes<T>(Func<Object> tester)
		where T : Exception
	{
		try {
			tester();
			return true;
		} catch (T) {
			return false;
		}
	}
	public static bool Successes(Func<Object> tester)
	{
		try {
			tester();
			return true;
		} catch {
			return false;
		}
	}
	public static async Task<bool> SuccessesAsync<T>(Func<Task> tester, int timeout)
		where T : Exception
	{
		try {
			return tester().Wait(timeout);
		} catch (T) {
			return false;
		}
	}
	public static async Task<bool> SuccessesAsync(Func<Task> tester, int timeout)
		=> await SuccessesAsync<Exception>(tester, timeout);
	public static async Task<Requester> ObtainTmpRequester()
	{
		Requester tmpreq = new Requester(Termui.SinglePrompt("Url del servidor"));
		if (await SuccessesAsync
				(tmpreq.GET_Objects<Project>, 10000))
			return tmpreq;
		Console.WriteLine("Error de red: No se puede alcanzar la dirección '{0}'", tmpreq.serverUrl);
		return await ObtainTmpRequester();
	}
	public static async Task<Requester> ObtainFirstTimeRequester()
	{
		Requester tmpreq = await ObtainTmpRequester();
		return await Requester.CreateFirstTimeRequester(
			tmpreq.serverUrl,
			await PromptUsername(tmpreq),
			(await SelectCharacter(tmpreq)).Name
		);
	}
	public static async Task<string> PromptUsername(Requester req){
		string name = Termui.SinglePrompt("Nombre de usuario");
		if (!(await SuccessesAsync
				(() => req.LookupUser(name), 10000)))
			return name;
		Console.WriteLine("Error: el usuario '{0}' ya existe.", name);
		return await PromptUsername(req);
	}
	public static Termui.EventOptionDisplayer SideDisplayer(Character[] availChars)
		=> (left, top, option) =>
			Termui.PrintWrapped(
				left,
				top,
				Console.BufferWidth - 4, Termui.comfortableHeight - 3, true,
				availChars.Single(c => c.Name == option).AsciiArt
			);
	public static Termui.EventOptionDisplayer SideDisplayer(Assignment[] assignments)
		=> (left, top, option) =>
			Termui.PrintWrapped(
				left,
				top,
				Console.BufferWidth - 4, Termui.comfortableHeight - 3, false,
				Termui.Colorize(assignments.Single(c => c.Uid.ToString() == option).ToString(),
				assignments.Single(c => c.Uid.ToString() == option).Rank)
			);
	public static Termui.EventOptionDisplayer SideDisplayer(Project[] projects)
		=> (left, top, option) =>
			Termui.PrintWrapped(
				left,
				top,
				Console.BufferWidth - 4, Termui.comfortableHeight - 3, false,
				projects.Single(p => p.Name == option).Opening
			);
	public static Termui.EventOptionDisplayer SideDisplayer(Requester req, User[] users)
		=> (left,top,option) => { Task.Run(async () =>
			Termui.PrintWrapped(
				left,
				top,
				Console.BufferWidth - 4, Termui.comfortableHeight - 3, true,
				"Camarada en: " + (await CommonProjects(req, option)) + "\n"
				+ users.Single(p => p.Name == option).Summary()
			)).Wait(); return 1;};
	public static Termui.EventOptionDisplayer SideDisplayer(AssignmentStats[] asstats)
		=> (left, top, option) =>
			Termui.PrintWrapped(
				left,
				top,
				Console.BufferWidth - 4, Termui.comfortableHeight - 3, false,
				PrintAssignmentStats(asstats.Single(ai => ai.Assigned.Uid.ToString() == option))
			);
	
	public static async Task<Character> SelectCharacter(Requester req){
		Character[] availableChars = await req.GET_Objects<Character>();
		Termui.EventOptionDisplayer displayer = SideDisplayer(
			availableChars.Append(new Character("Subir el tuyo", "+")).ToArray());
		Termui.OptionDisplayer += displayer;
		Termui.banner = "Por favor selecciona un personaje: ";
		int selection = Termui.Menu(
			availableChars.Select(c => c.Name)
			.Append("Subir el tuyo")
			.ToArray());
		Termui.OptionDisplayer -= displayer;
		return (selection < availableChars.Length)
			? availableChars[selection]
			: (Character) (await CreateObject<Character>(req));
	}

	public static string EditExternally(string msg)
	{
		// This is Windows-Compatible right?
		string tempDir = Environment.GetEnvironmentVariable("TEMP");
		tempDir = string.IsNullOrEmpty(tempDir) ? "/tmp" : tempDir;
		string tmpFilename = tempDir + "/" + "unicrawl-edit.txt";
		File.WriteAllText(tmpFilename, "# " + msg + " - las líneas que empiecen con '#' serán descartadas");
		Process editor = Process.Start(tmpFilename);
		editor.WaitForExit();
		string rel = string.Join('\n',
			File.ReadAllLines(tmpFilename).Where(l
				=> !l.StartsWith('#')
				&& !string.IsNullOrEmpty(l.Trim()))
		);
		if (!string.IsNullOrEmpty(rel))
			return rel;
		Console.WriteLine("Archivo vacío, intentando de nuevo.");
		return EditExternally(msg);
	}

	// UI VIEWS
	public static (string, Func<Requester, Task<bool>>)[] UserMenu =
		new (string, Func<Requester, Task<bool>>)[] {
			("Tus tareas", ViewAssignments),
			("Estadísticas de usuario", PrintUserInfo),
			("Tabla de posiciones", ViewLeaderboard),
			("Suscribirse a un proyecto", SubscribeMenu),
			("Crear un proyecto - sé headmaster!", (async (req) => await CreateObject<Project>(req) != null)),
		};
	public static (string, Func<Requester, Task<bool>>)[] MasterMenu =
		new (string, Func<Requester, Task<bool>>)[] {
			("Tus tareas", ViewAssignments),
			("Estadísticas de usuario", PrintUserInfo),
			("Tabla de posiciones", ViewLeaderboard),
			("Suscribirse a un proyecto", SubscribeMenu),
			("Publicar tarea", CreateAssignment),
			("Estadísticas de proyecto", ViewProjectInfo)
		};
	
	public static (string, Func<Requester, Task<bool>>)[] MenuSelect(Requester req)
		=> req.LoggedUser.MasterOf == null ? UserMenu : MasterMenu;

	public static async Task<bool> MainMenu(Requester req, bool shouldGreet)
	{
		Termui.banner = string.Format("Bienvenido, {0:s}\n", req.LoggedUser.Name);
		int sel;
		return (sel = Termui.Menu(
			MenuSelect(req)
				.Select(t => t.Item1)
				.Append("Quit")
				.ToArray()
		)) < MenuSelect(req).Length
		&& await (MenuSelect(req)[sel].Item2)(req)
		&& await MainMenu(req, false);
	}
	public static async Task<bool> ViewAssignments(Requester req){
		if ( req.LoggedUser.Assignments.Length == 0 ){
			Termui.Notification("Sin tareas para mostrar.");
			return true;
		}
		Termui.EventOptionDisplayer displayer = SideDisplayer(req.LoggedUser.Assignments);
		Termui.OptionDisplayer += displayer;
		string sel;
		Termui.banner = "Estas son tus tareas: ";
		while ( (sel = Termui.ColorTable(-1, 1, true,
				new string[]{"Uid", "Estado", "Proyecto", "Rango", "Fecha límite", "Título"},
				() => req.LoggedUser.Assignments.Select(a
					=> new string[] { a.Uid.ToString(),
					                  a.DateComplete != null ? "C" : " ",
					                  a.Project,
					                  a.Rank.ToString(),
					                  a.DueDate.ToShortDateString(),
					                  a.Title }
				).ToArray()
				, 3 )) != "" )
			if ( req.LoggedUser.Assignments.Single(a => a.Uid.ToString() == sel).DateComplete != null )
				Termui.Notification("Esa tarea ya esta completa!");
			else if ( Termui.SinglePrompt("Quieres completar esta tarea? (y/n)")
			           .ToLower()[0] == 'y' )
				await CompleteAssignment(req, sel);
		Termui.OptionDisplayer -= displayer;
		return true;
	}

	public static async Task CompleteAssignment(Requester req, string uid)
	{
		await req.POST_completeAssignment(uid);
		Console.WriteLine("Tarea completada");
	}

	public static string sideBySide(string s1, string s2)
	{
		string[] spl1, spl2;
		(spl1, spl2) = (s1.Split("\n"), s2.Split("\n"));
		int maxlen = spl1.Max(l => l.Length);
		string tmp;
		return string.Join("\n",
			Enumerable.Range(0, Math.Max(spl1.Length, spl2.Length))
				.Select(i =>
					  (tmp = ((i < spl1.Length) ? spl1[i] : ""))
					+ string.Join("", Enumerable.Repeat(" ", maxlen - tmp.Length + 2))
					+ (i < spl2.Length ? spl2[i] : "")
			)
		);
	}

	public static async Task<bool> PrintUserInfo(Requester req)
	{
		PrintUserInfo(req.LoggedUser);
		/*
		Console.WriteLine(sideBySide(
			req.LoggedUser.CharType.AsciiArt,
			  req.LoggedUser.ToString()
			+ "\n\nKills: \n" +
			string.Join("\n",
				req.LoggedUser.Kills().Select(t => string.Format("{0,15:s}, {1,-3}", t.Item1, t.Item2))
			)
		));
		*/
		return true;
	}
	
	public static async void PrintUserInfo(User u)
	{
		Console.WriteLine(sideBySide(
			u.CharType.AsciiArt,
			  u.ToString()
			+ "\n\nKills: \n" +
			string.Join("\n",
				u.Kills().Select(t => string.Format("{0,15:s}, {1,-3}", t.Item1, t.Item2))
			)
		));
		Console.ReadKey(true);
	}

	public static async Task<string> CommonProjects(Requester req, string otherUsername)
		=> string.Join(", ", Enumerable.Intersect(
			(await req.GET_Projects(req.LoggedUser.Name, true)).Select(p => p.Name),
			(await req.GET_Projects(otherUsername, true)).Select(p => p.Name)
		));
	public static async Task<bool> ViewLeaderboard(Requester req){
		if ( req.Peers.Length == 0 ){
			Termui.Notification("Parece que no tienes camaradas. Suscríbete a más projectos!");
			return true;
		}
		Termui.EventOptionDisplayer displayer = SideDisplayer(req, req.Peers);
		Termui.EventOptionDisplayer peerAssignmentObtainer = (x,y,name) => req.SchedulePeerLookup(name);
		Termui.OptionDisplayer += displayer;
		Termui.OptionDisplayer += peerAssignmentObtainer;
		Termui.banner = req.Peers.Length + " amigos: ";
		string sel;
		while ( (sel = Termui.ColorTable(-1, 1, true,
			new string[]{"Name", "Xp", "Lvl", "Nombre"},
			() => req.Peers.Select(u
				=> new string[] { u.Name, u.Xp.ToString(), u.Level.ToString(), u.Name }
			).ToArray(), 2
		)) != ""){
			Console.Clear();
			PrintUserInfo(await ((Task<User>) (req.ScheduledTasks.Pop())));
		}
		Termui.OptionDisplayer -= displayer;
		Termui.OptionDisplayer -= peerAssignmentObtainer;
		return true;
	}

	public static async Task<bool> SubscribeMenu(Requester req)
	{
		Project[] availableProjects = await req.GET_Projects(req.LoggedUser.Name, false);
		if ( availableProjects.Length <= 0 ){
			Termui.Notification("No hay proyectos disponibles");
			return true;
		}
			
		Termui.EventOptionDisplayer displayer = SideDisplayer(availableProjects);
		Termui.OptionDisplayer += displayer;
		Console.WriteLine("Elije un proyecto para suscribirte: ");
		await req.POST_subscribeProj(
			availableProjects[
				Termui.Menu(
					availableProjects.Select(p => p.Name).ToArray()
				)].Name);
		Termui.OptionDisplayer -= displayer;
		return true;
	}

	public static async Task<T> CreateObject<T>(Requester req)
		where T : IPubliclyCreatable
	{
		IPubliclyCreatable rel;
		string name = Termui.SinglePrompt(typeof(T).Name + " name");
		if ( (await req.GET_Objects<T>()).Select( p => p.Name).Contains(name) ){
			Console.WriteLine(typeof(T).Name + " '{0}' ya existe, por favor utiliza otro nombre.", name);
			return await CreateObject<T>(req);
		}
		rel = typeof(T).Name switch {
			"Character" => 
				new Character( name, EditExternally("Por favor suministra el arte ASCII del personaje")),
			"Project" =>
				new Project( name, EditExternally("Por favor suministra la descripción del proyecto")),
			_ => throw new Exception()
		};
		await req.POST_newObject<T>((T) rel);
		if ( req.LoggedUser != null )
			await req.RetrieveLoggedUser();
		return (T) rel;
	}

	public static async Task<bool> CreateAssignment(Requester req)
	{
		string[] inputVals = Termui.MultiPrompt(
			new string[]{"Título", "Fecha límite (dd/MM/yyyy)", "Rango"},
			new bool[]{false, false, false}
		);

		if ( !Successes((() => Dater.ParseDate(inputVals[1]))) ){
			Console.WriteLine("Fecha inválida: " + inputVals[1]);
			return await CreateAssignment(req);
		} 
		if ( !string.IsNullOrEmpty(inputVals[2]) && !Successes(() => byte.Parse(inputVals[2])) ){
			Console.WriteLine("Rango inválido: " + inputVals[2]);
			return await CreateAssignment(req);
		}

		await req.POST_newObject<Assignment>(
			new Assignment(
				inputVals[0],
				EditExternally("Por favor suministra la descripción de la tarea"),
				await SelectCharacter(req),
				string.IsNullOrEmpty(inputVals[2]) ? (byte) 0 : byte.Parse(inputVals[2]),
				Dater.ParseDate(inputVals[1]),
				"",
				Guid.Empty,
				null
			));
		return true;
	}

	public static string PrintAssignmentStats(AssignmentStats ai)
		=> "Completado por:\n"
		+ string.Join("\n",
			ai.CompletedBy.Select(v =>
			string.Format("{0,12:s} - {1:s}", v.Item1.ToShortDateString(), v.Item2.Name)
		));
	public static async Task<bool> ViewProjectInfo(Requester req)
	{
		ProjectStats info = await req.GET_projectInfo(req.LoggedUser.MasterOf);
		Termui.EventOptionDisplayer displayer = SideDisplayer(info.AssignmentInfo);
		Termui.OptionDisplayer += displayer;
		Termui.banner = info.ToString();
		while (
		Termui.ColorTable(-1, 1, true,
			new string[]{ "uid", "Título", "Rango", "Completado por", "Asignado a" },
			() => info.AssignmentInfo.Select(
				ai => new string[]{ ai.Assigned.Uid.ToString()
				                   ,ai.Assigned.Title
				                   ,ai.Assigned.Rank.ToString()
				                   ,ai.CompletedBy.Length.ToString()
				                   ,ai.AssignedTo.Length.ToString()
				                  }).ToArray(), 2
		) != "");
		Termui.OptionDisplayer -= displayer;
		return true;
	}

	public static async void NotifyUserUpdate(object sender, UserUpdateEventArgs e)
	{
		string msg = "";
		try {
			msg +=         (e.ExperienceGain > 0) ? $"Has ganado {e.ExperienceGain} experiencia" : "";
			msg +=              (e.LevelGain > 0) ? $"\nFelicitaciones, has subido de nivel!" : "";
			msg += (e.NewAssignments.Length > 0)  ? "\nTienes nuevas tareas pendientes en: "
				+ string.Join(", ", e.NewAssignments.Select(a => a.Project)) : "";
		} catch {
			return;
		}
		if ( msg != "" ){
			Termui.Notification(msg);
		}
	}

	public static async Task Main()
	{
		try {
			Requester req = await ObtainRequester();
			File.WriteAllText(
				configFile,
				JsonConvert.SerializeObject(req.ObtainConfig())
			);
			// wait for User information to be obtained
			while (req.ScheduledTasks.Count > 0)
				await req.ScheduledTasks.Pop();
			req.BackgroundSyncEnabled = true;
			req.UserUpdate += NotifyUserUpdate;
			await Client.MainMenu(req, true);
		} catch (HttpRequestException e) {
			Console.WriteLine("Error de red, por favor asegúrate de tener una conexión estable.");
		}
	}
}
