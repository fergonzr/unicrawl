all: uniclient uniserver
uniclient:
	mono-csc -r:System.Net.Http.dll -r:Newtonsoft.Json.dll -r:Mono.Data.Sqlite.dll *.cs client/*.cs -out:uniclient.exe
uniserver:
	mono-csc -r:System.Net.Http.dll -r:Newtonsoft.Json.dll -r:Mono.Data.Sqlite.dll *.cs server/*.cs -out:uniserver.exe

clean: all
	rm *.db

test-%: 
	mono-csc -r:System.Net.Http.dll -r:Newtonsoft.Json.dll -r:Mono.Data.Sqlite.dll *.cs client/*.cs server/*.cs tests/$*.cs -out:test-$*.exe
