using System;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

interface IPubliclyCreatable {
	public abstract string Name {get;}
}

class AssignmentComparer : IEqualityComparer<Assignment> {
	public bool Equals(Assignment t, Assignment f) => t.Uid == f.Uid;
	public int GetHashCode(Assignment t) => t.Uid.GetHashCode();
}

class User {
	public string Name {get;}
	public int Xp {get;}
	public int Level { get {
		if (Xp < 8)
			return 0;
		if (Xp < 48)
			return 1;
		int level = 2;
		while ( Xp >= 8 + 40 * Enumerable.Range(0,level).Select(n => Math.Pow(2,n)).Sum())
			level++;
		return level;
	}}
	public string? MasterOf {get;}
	public Character CharType {get;}
	public Assignment[] Assignments;
	[JsonConstructor]
	public User(string name, int xp, string? masterOf, Character charType, Assignment[] assignments )
	{
		this.Name = name;
		this.Xp = xp;
		this.MasterOf = masterOf;
		this.CharType = charType;
		this.Assignments = assignments;
	}
	public User(string name, Character charType)
	{
		this.Name = name;
		this.CharType = charType;
		this.Xp = 0;
	}
	public User(IDataRecord record, Assignment[] tasklist)
	{
		this.Name       = record.GetString(record.GetOrdinal("username"));
		this.Xp         = record.GetInt32(record.GetOrdinal("xp"));
		try {
			this.MasterOf  = record.GetString(record.GetOrdinal("masterOf"));
		} catch {}
		this.CharType  = new Character(record);
		Assignments = tasklist;
	}

	public override string ToString()
		=> string.Format("{0:s}, {1:s} de nivel {2:d} ({3:d}xp)" +
			( (this.MasterOf != null) ? "\nHead master de {4:d}." : "." ),
				this.Name, this.CharType.Name, this.Level, this.Xp, this.MasterOf
			);
	public (string, int)[] Kills()
		=> Assignments
			.Where(a => a.DateComplete != null)
			.GroupBy(a => a.Project)
			.Select(g => (g.Key, g.Count()))
			.ToArray();
	public string Summary()
		=> this.ToString() + "\n\n" + this.CharType.AsciiArt;
}

class AuthUser : User {
	private const byte tokenLen = 50;
	// Map a byte to an alphanumeric char
	private char charisize(byte b)
	{
		return (char) ((b % 60) switch {
			// Numbers
			< 10 => (b % 60) + '0',
			// Uppercase letters
			< 35 => (b % 60) - 10 + 'A',
		// Lowercase letters
			_ => (b % 60) - 35 + 'a'
		});
	}
	// Get a new cryptographically secure random alphanumeric string
	private string randomStr(int len)
	{
		byte[] rndBytes = new byte[len];
		using RNGCryptoServiceProvider rndgen = new RNGCryptoServiceProvider();
		rndgen.GetBytes(rndBytes);
		return new string(rndBytes.Select(b => charisize(b)).ToArray());
	}
	public string Token;
	public AuthUser(string name, Character charType) : base(name, charType)
	{
		this.Token = randomStr(tokenLen);
	}
	public AuthUser(User u, string token) : base(u.Name, u.Xp, u.MasterOf, u.CharType, u.Assignments)
	{
		this.Token = token;
	}
	[JsonConstructor]
	public AuthUser(string name, int xp, string? masterOf, Character charType, Assignment[] assignments, string token )
		: base(name, xp, masterOf, charType, assignments)
	{
		this.Token = token;
	}
}

class Assignment {
	public Guid Uid {get; }
	public string Title {get;}
	public string Description {get;}
	public Character Enemy {get;}
	public byte Rank {get;}
	public DateTime DueDate {get;}
	public string Project {get;}
	public DateTime? DateComplete {get;}
	public TimeSpan TimeLeft { get => this.DueDate - DateTime.Now;}
	[JsonConstructor]
	public Assignment(string title, string description, Character enemy, byte rank, DateTime dueDate, string project, Guid uid, DateTime? dateComplete)
	{
		this.Title = title;
		this.Description = description;
		this.Enemy = enemy;
		this.Rank = rank;
		this.DueDate = dueDate;
		this.Project = project;
		this.Uid = uid;
		this.DateComplete = dateComplete;
	}
	public Assignment(IDataRecord record)
	{
		this.Uid = new Guid(record.GetString(record.GetOrdinal("Uid")));
		this.Title  = record.GetString(record.GetOrdinal("title"));
		this.Description = record.GetString(record.GetOrdinal("description"));
		this.Enemy = new Character(record);
		this.Rank = record.GetByte(record.GetOrdinal("weight"));
		this.DueDate = DateTime.Parse(record.GetString(record.GetOrdinal("duedate")));
		this.Project = record.GetString(record.GetOrdinal("projectname"));
		try {
			this.DateComplete = DateTime.Parse(record.GetString(record.GetOrdinal("dateComplete")));
		} catch {}
	}
	public override string ToString()
		=> $"{this.Title} (Lvl {this.Rank}):\n{this.Description}\n\n{this.Enemy.AsciiArt}";
	public override bool Equals(Object obj)
		=> (obj != null) && (obj is Assignment)
		&& this.Uid.Equals(((Assignment) obj).Uid);
	public override int GetHashCode()
		=> this.Uid.GetHashCode();
}

class Project : IPubliclyCreatable {
	public string Name {get;}
	public string Opening {get;}
	[JsonConstructor]
	public Project(string name, string opening)
	{
		this.Name = name;
		this.Opening = opening;
	}
	public Project(IDataRecord record)
	{
		this.Name = record.GetString(0);
		this.Opening = record.GetString(1);
	}
}

class Character : IPubliclyCreatable {
	public string Name {get;}
	public string AsciiArt {get;}
	public Character(IDataRecord record)
	{
		this.Name = record.GetString(record.GetOrdinal("name"));
		this.AsciiArt = record.GetString(record.GetOrdinal("asciiArt"));
	}
	[JsonConstructor]
	public Character(string name, string asciiArt)
	{
		this.Name = name;
		this.AsciiArt = asciiArt;
	}
}

class AssignmentStats {
	public Assignment Assigned {get;}
	public (DateTime?, User)[] AssignedTo {get;}
	[JsonIgnore]
	public (DateTime, User)[] CompletedBy { get
		=> Array.FindAll(AssignedTo, v => v.Item1 != null)
	            .Select(v => ((DateTime) v.Item1, v.Item2)).ToArray();
	       }
	

	[JsonConstructor]
	public AssignmentStats (Assignment assigned, (DateTime?, User)[] assignedTo)
	{
		this.Assigned = assigned;
		this.AssignedTo  = assignedTo;
	}
	public AssignmentStats (IDataReader reader)
	{
		List<(DateTime?, User)> assignedTo = new List<(DateTime?, User)>();
		this.Assigned = new Assignment((IDataRecord) reader);
		do {
			assignedTo.Add(
				( (reader.IsDBNull(reader.GetOrdinal("dateComplete"))
				? null
				: DateTime.Parse(reader.GetString(reader.GetOrdinal("dateComplete"))))
				, new User((IDataRecord) reader, null))
			);
		} while(reader.Read()
			&& this.Assigned.Uid.ToString().Equals(
			   reader.GetString(reader.GetOrdinal("Uid"))
			));
		this.AssignedTo = assignedTo.ToArray();
	}
}

class ProjectStats {
	public AssignmentStats[] AssignmentInfo {get;}
	public User[] SubscribedUsers {get;}
	public Project StatedProject {get;}

	[JsonConstructor]
	public ProjectStats(AssignmentStats[] assignmentInfo, User[] subscribedUsers, Project statedProject)
	{
		this.AssignmentInfo = assignmentInfo;
		this.SubscribedUsers  = subscribedUsers;
		this.StatedProject  = statedProject;
	}

	public override string ToString()
		=> string.Format("{0:s}:\n\"{1:s}\"\n{2:d} Usuarios suscritos:\n{3:s}\n\n{4:d} Tareas:",
			this.StatedProject.Name,
			this.StatedProject.Opening,
			this.SubscribedUsers.Length,
			string.Join("\n", this.SubscribedUsers.Select(u => u.Name)),
			this.AssignmentInfo.Length
		);
}
