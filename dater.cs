using System;
using System.Linq;
using System.Globalization;
class NonDateException : Exception {

}
class EarlierDateException : Exception {

}
class Dater {
	public static string[] daynames = new string[] { "sun", "mon", "tue", "wed", "thu", "fri", "sat" };
	
	public static DateTime ParseDate(string s)
	{
		try {
			DateTime d = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
			if ( d.CompareTo(DateTime.Today) < 0 )
				throw new EarlierDateException();
			return d;
		} catch (FormatException) {
			try {
				return Dater.ParseRel(s);
			} catch {
				throw new NonDateException();
			}
		}
	}
	public static DateTime GetRelTime(int multiplier, DayOfWeek dow)
	{
		DateTime cur = DateTime.Today.AddDays(1);
		for (;multiplier > 0; cur = cur.AddDays(1))
			if (cur.DayOfWeek == dow)
				multiplier--;
		return cur.AddDays(-1);
	}
	public static DateTime ParseRel(string timespec)
	{
			DayOfWeek dow = (DayOfWeek) Array.FindIndex(
				daynames, d => timespec.ToLower().EndsWith(d)
			);
			return GetRelTime(
				Char.IsNumber(timespec[0]) ?
					int.Parse(new String(
						timespec.TakeWhile(c => Char.IsNumber(c)).ToArray()
					)) : 1,
				dow
			);
	}
}
