FROM mono:latest
RUN mkdir /opt/app
COPY uniserver.exe /opt/app
COPY Newtonsoft.Json.dll /opt/app
EXPOSE 8080
CMD ["mono", "/opt/app/uniserver.exe"]