using Mono.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

abstract class SingleValueException : Exception {
	public string val {get;}
	public SingleValueException(string val){
		this.val = val;
	}
}

class NonUniqueFieldException : SingleValueException {
	public NonUniqueFieldException(string val) : base(val) {}
}
class RecordNotFoundException : SingleValueException {
	public RecordNotFoundException(string val) : base(val) {}
}
class MakeYourOwnProjectFirstException : SingleValueException {
	public MakeYourOwnProjectFirstException(string val) : base(val) {}
}
class AccessDeniedException : Exception {}

class DBQuerier : IDisposable {
	private const string connectionString = "URI=file:unicrawl.db";
	private const int tokenLen = 50;
	private IDbConnection dbcon;
	private Encoding encoding = new UTF8Encoding();
	
	public DBQuerier(){
		dbcon = new SqliteConnection(connectionString);
		dbcon.Open();
		if ( this.dbIsEmpty() )
			this.dbInit();
	}
	private IDataReader exec(string strcmd)
	{
		IDbCommand dbcmd = dbcon.CreateCommand();
		dbcmd.CommandText = strcmd;
		return dbcmd.ExecuteReader();
	}
	private bool dbIsEmpty()
	{
		try {
			exec("SELECT * FROM usr2task limit 1;").Dispose();
			return false;
		} catch (SqliteException){
			return true;
		}
	}
	private void dbInit()
	{
		exec(@"CREATE TABLE characters(
			id integer primary key autoincrement not null,
			name varchar(100) not null,
			asciiArt text not null
		);").Dispose();
		exec(@"CREATE TABLE projects(
			id integer primary key autoincrement not null,
			name varchar(100) not null,
			openingText text not null
		);").Dispose();
		exec(@"CREATE TABLE tasks(
			id integer primary key autoincrement not null,
			uid char(36) not null,
			title varchar(100) not null,
			description text,
			weight tinyint not null,
			duedate varchar(22),
			character int references characters(id) not null,
			project int references projects(id) not null
		);").Dispose();
		exec(@"CREATE TABLE users(
			id integer primary key autoincrement not null,
			name varchar(100) not null,
			xp int not null default 0,
			hash binary(8) not null,
			character int references characters(id) not null,
			masterOf int references projects(id)
		);").Dispose();
		exec(@"CREATE TABLE usr2proj(
			userid int references users(id) not null,
			projid int references projects(id) not null,
			UNIQUE(userid, projid)
		);").Dispose();
		exec(@"CREATE TABLE usr2task(
			userid int references users(id) not null,
			dateComplete varchar(22),
			taskid int references tasks(id) not null
		);").Dispose();
		// Triggers
		exec(@"CREATE TRIGGER relate_usr2tasks AFTER INSERT ON tasks
			BEGIN
				INSERT INTO usr2task (userid, taskid) 
				SELECT userid, new.id FROM usr2proj WHERE projid = new.project;
			END;
		").Dispose();
		/* Update the experience of a player on task completion,
		 * proportionally to the rank of the completed task and
		 * the square root of the hours left before its due date.
		 */
		exec(@"CREATE TRIGGER assignExp UPDATE OF datecomplete ON usr2task
			BEGIN
				UPDATE users SET xp = xp + (SELECT
					8*(t.weight + 1)
					+ round(sqrt(max(
						(unixepoch(t.duedate) - unixepoch(new.dateComplete))/3600.0
					,0)))
				FROM tasks t WHERE t.id = new.taskid LIMIT 1) WHERE id = new.userid;
			END;").Dispose();
	}
	private string sanitize(string s) => s
		.Replace("'", "''")
		.Replace("\"", "\"\"");
	private bool existsRecord(string token, string colname, string table)
	{
		using IDataReader reader = exec($"SELECT {colname} FROM {table} WHERE {colname} = '{sanitize(token)}' LIMIT 1;");
		reader.Read();
		return !reader.IsDBNull(0);
	}
	private string hash(string token) =>
		Hex.Bytes2Hex(SHA256.Create()
			.ComputeHash(encoding.GetBytes(token)));
	private Guid getUniqueGuid()
	{
		Guid uid;
		do
			uid = Guid.NewGuid();
		while (existsRecord(sanitize(uid.ToString()), "uid", "tasks"));
		return uid;
	}
	private bool checkAuth(string name, string token)
	{
		using IDataReader reader = exec(
			$@"SELECT name FROM users
			WHERE name = '{sanitize(name)}'
			AND hash = '{sanitize(hash(token))}' LIMIT 1;"
		);
		return reader.Read();
	}
	private bool hasProject(string username)
	{
		using IDataReader reader = exec($"SELECT masterOf FROM users WHERE name = '{sanitize(username)}';");
		reader.Read();
		return !reader.IsDBNull(0);
	}
	private bool hasAssignment(string uid, string username)
	{
		using IDataReader reader = exec(
			$@"SELECT name FROM users u
			INNER JOIN usr2task ON userid = u.id
			INNER JOIN tasks t ON taskid = t.id
			WHERE t.uid = '{sanitize(uid)}'
			AND u.name = '{sanitize(username)}'
			LIMIT 1;"
		);
		return reader.Read();
	}
	public void NewCharacter(string name, string asciiArt)
	{
		if ( existsRecord(sanitize(name), "name", "characters") )
			throw new NonUniqueFieldException(name);
		exec($@"
			INSERT INTO characters (name, asciiArt) VALUES (
			'{sanitize(name)}', '{sanitize(asciiArt)}'
		);").Dispose();
	}
	public Character[] GetCharacters()
	{
		using IDataReader reader = exec($"SELECT name, asciiArt FROM characters;");
		List<Character> charList = new List<Character>();
		while (reader.Read())
			charList.Add(
				new Character ((IDataRecord) reader)
			);
		return charList.ToArray();
	}
	public Character GetCharacter(string name)
	{
		using IDataReader reader = exec($"SELECT name, asciiArt FROM characters WHERE name = '{sanitize(name)}' LIMIT 1;");
		return reader.Read() ? new Character((IDataRecord) reader) : null;
	}
	public AuthUser NewUser(string name, string selchar)
	{
		if ( existsRecord(sanitize(name), "name", "users") )
			throw new NonUniqueFieldException(name);
			
		Character c = GetCharacter(selchar);
		if ( c == null )
			throw new RecordNotFoundException(selchar);
		AuthUser u = new AuthUser(name, c);

		string query = "INSERT INTO users (name, hash, character) "
			+ $"VALUES( '{sanitize(u.Name)}', '{sanitize(hash(u.Token))}',"
			+ $"(SELECT id FROM characters WHERE name = '{sanitize(selchar)}'));";
		exec(query).Dispose();
		return u;
	}
	// Get the info of a specific user, including his assigned tasks
	public User GetUser(string name)
	{
		string query = $@"SELECT u.name as username, u.xp, c.name, c.asciiArt, p.name as masterOf
			FROM users u
			INNER JOIN characters c ON u.character = c.id
			LEFT  JOIN projects p ON u.masterOf = p.id
			WHERE u.name = '{sanitize(name)}';";
		using IDataReader reader = exec(query);
		return reader.Read() ? new User((IDataRecord) reader, GetAssignments(name, null, null)) : null;
	}
	public User[] GetUsers(string project)
	{
		string query = $@"SELECT u.name as username, u.xp, c.name, c.asciiArt, (SELECT name FROM projects WHERE id = u.masterOf LIMIT 1) AS masterOf
			FROM usr2proj
			INNER JOIN users u ON userid = u.id
			INNER JOIN projects p ON projid = p.id
			INNER JOIN characters c ON u.character = c.id
			WHERE p.name = '{sanitize(project)}';";
		using IDataReader reader = exec(query);
		List<User> entryList = new List<User>();
		while (reader.Read())
			entryList.Add(
				new User((IDataRecord) reader, null)
			);
		return entryList.ToArray();
	}
	public User[] GetFriends(string user)
	{
		string query = $@"
			SELECT u.name as username, u.xp, c.name, c.asciiArt,
				(SELECT name FROM projects WHERE id = u.masterOf LIMIT 1) AS masterOf
			FROM usr2proj
			INNER JOIN users u ON u.id = userid
			INNER JOIN
			( SELECT projid AS seekedProj
				FROM users u
				INNER JOIN usr2proj
				INNER JOIN projects p
				WHERE u.name = '{sanitize(user)}'
			) ON seekedProj = projid
			INNER JOIN characters c ON c.id = u.character
			GROUP BY u.id
			HAVING u.id != (SELECT id FROM users WHERE name = '{sanitize(user)}');";
		using IDataReader reader = exec(query);
		List<User> entryList = new List<User>();
		while (reader.Read())
			entryList.Add(
				new User((IDataRecord) reader, null)
			);
		return entryList.ToArray();
	}
	public Project NewProject(string name, string token, string projectname, string projectopen)
	{
		if ( existsRecord(projectname, "masterOf", "users") ||
		     existsRecord(projectname, "name", "projects") )
			throw new NonUniqueFieldException(projectname);

		if ( !checkAuth(name, token) )
			throw new AccessDeniedException();
		exec($@"INSERT INTO projects (name, openingText) VALUES "
			+ $"('{sanitize(projectname)}', '{sanitize(projectopen)}');"
		).Dispose();
		exec("UPDATE users SET masterOf = "
			+ $"(SELECT id FROM projects WHERE name = '{sanitize(projectname)}') "
			+ $"WHERE name = '{sanitize(name)}' AND hash = '{sanitize(hash(token))}';"
		).Dispose();
		return new Project(projectname, projectopen);
	}
	public Project GetProject(string projectname)
	{
		using IDataReader reader = exec($@"
			SELECT name,openingText FROM projects
			WHERE name = '{sanitize(projectname)}';");
		if (reader.Read())
			return new Project((IDataRecord) reader);
		return null;
	}
	public Project[] GetProjects()
	{
		using IDataReader reader = exec($"SELECT name,openingText FROM projects;");
		List<Project> entryList = new List<Project>();
		while (reader.Read())
			entryList.Add(
				new Project((IDataRecord) reader)
			);
		return entryList.ToArray();
	}
	// Retrieve the projects a given user is (or is not) subscribed to
	public Project[] GetProjects(string user, bool isSubscribed)
	{
		using IDataReader reader = exec(
			$@"SELECT p.name,p.openingText FROM projects p
			   LEFT JOIN usr2proj ON p.id = projid
			   GROUP BY p.id
			   HAVING MAX(CASE  WHEN userid =
				   (SELECT id FROM users WHERE name = '{sanitize(user)}')
			   THEN 1 ELSE 0 END) = " + (isSubscribed ? "1;" : "0;"));
		return listRecords(reader, r => new Project(r));
	}
	public void SubscribeProj(string user, string token, string projname)
	{
		if ( !checkAuth(user, token) )
			throw new AccessDeniedException();
		if ( !existsRecord(projname, "name", "projects") )
			throw new RecordNotFoundException(projname);
		string query = $@"INSERT OR IGNORE INTO usr2proj VALUES (
			(SELECT id FROM users WHERE name = '{sanitize(user)}' AND hash = '{sanitize(hash(token))}' LIMIT 1),
			(SELECT id FROM projects WHERE name = '{sanitize(projname)}' LIMIT 1)
		);";
		exec(query).Dispose();
	}
	public Guid NewAssignment(string title, string desc, string enemy, byte rank, DateTime dueDate,
	                    string user, string token)
	{
		if ( !checkAuth(user, token) )
			throw new AccessDeniedException();
		if ( !existsRecord(enemy, "name", "characters") )
			throw new RecordNotFoundException(enemy);
		if ( !hasProject(user) )
			throw new MakeYourOwnProjectFirstException(user);
		Guid uid = getUniqueGuid();
		string query = $@"INSERT INTO tasks (uid, title, description, weight, duedate, character, project) VALUES (
			'{uid.ToString()}',
			'{sanitize(title)}',
			'{sanitize(desc)}',
			{rank},
			'"+ dueDate.ToString("o") + $@"',
			(SELECT id FROM characters WHERE name = '{sanitize(enemy)}' LIMIT 1),
			(SELECT masterOf FROM users WHERE
			name = '{sanitize(user)}' AND hash = '{sanitize(hash(token))}' LIMIT 1)
		);";
		exec(query).Dispose();
		return uid;
	}
	public Assignment GetAssignment(string uid)
	{
		string query = $@"SELECT
			t.uid, t.title, t.description, c.name, c.asciiArt, t.weight, t.duedate, p.name as projectname
			FROM tasks t INNER JOIN characters c
			ON t.character = c.id
			INNER JOIN projects p
			ON t.project = p.id
			WHERE t.uid = '{sanitize(uid)}';";
		using IDataReader reader = exec(query);
		return reader.Read() ? new Assignment((IDataRecord) reader) : null;
	}
	public Assignment[] GetAssignments(string project)
	{
		string query = $@"SELECT
			t.uid, t.title, t.description, c.name, c.asciiArt, t.weight, t.duedate, p.name as projectname
			FROM tasks t INNER JOIN characters c
			ON t.character = c.id
			INNER JOIN projects p
			ON t.project = p.id
			WHERE p.name = '{sanitize(project)}';";
		using IDataReader reader = exec(query);
		List<Assignment> entryList = new List<Assignment>();
		while (reader.Read())
			entryList.Add(
				new Assignment((IDataRecord) reader)
			);
		return entryList.ToArray();
	}
	public Assignment[] GetAssignments(string? user, bool? completed, string? project)
	{
		string query = $@"SELECT
			t.uid, t.title, t.description, c.name, c.asciiArt, t.weight, t.duedate, p.name as projectname, dateComplete
			FROM tasks t INNER JOIN usr2task ON taskid = t.id
			INNER JOIN characters c
			ON t.character = c.id
			INNER JOIN projects p
			ON t.project = p.id
			WHERE TRUE"
			+ ( !string.IsNullOrEmpty(user) ? $@" AND userid = (SELECT id FROM users
				WHERE name = '{sanitize(user)}')" : "" )
			+ ( (completed != null) ? (" AND dateComplete IS " + ((bool) completed ? "NOT" : "") + " NULL" ) : "")
			+ ( !string.IsNullOrEmpty(project) ? $" AND p.name = '{sanitize(project)}'" : "")
		+ ";";
		using IDataReader reader = exec(query);
		List<Assignment> entryList = new List<Assignment>();
		while (reader.Read())
			entryList.Add(
				new Assignment((IDataRecord) reader)
			);
		return entryList.ToArray();
	}
	private T[] listRecords<T>(IDataReader reader, Func<IDataRecord, T> obtainer)
	{
		List<T> recordList = new List<T>();
		while (reader.Read())
			recordList.Add( obtainer((IDataRecord) reader) );
		return recordList.ToArray();
	}
	public AssignmentStats[] AssignmentInfo(string project)
	{
		using IDataReader reader = exec($@"
			SELECT t.uid, t.title, t.description, c.name, c.asciiArt, t.weight, t.duedate, p.name as projectname
			     , u.name as username, u.xp, p.name as masterOf, dateComplete
			FROM tasks t
			INNER JOIN usr2task ON taskid = t.id
			INNER JOIN characters c ON t.character = c.id
			INNER JOIN projects p ON t.project = p.id
			INNER JOIN users u ON userid = u.id
			WHERE p.name = '{sanitize(project)}'
			ORDER BY t.uid;");

		if (!reader.Read())
			return new AssignmentStats[0];
		List<AssignmentStats> assignmentInfo = new List<AssignmentStats>();
		while (true)
			try {
				assignmentInfo.Add( new AssignmentStats(reader) );
			} catch (InvalidOperationException) {
				break;
			}
		return assignmentInfo.ToArray();
	}
	public ProjectStats ProjectInfo(string project)
	{
		return new ProjectStats(AssignmentInfo(project), GetUsers(project), GetProject(project));
	}
	public void CompleteAssignment(string username, string token, string tuid)
	{
		if ( !checkAuth(username, token) )
			throw new AccessDeniedException();
		if ( !hasAssignment(tuid, username) )
			throw new RecordNotFoundException(tuid);
		string query = "UPDATE usr2task SET dateComplete = '" + DateTime.Now.ToString("o") + "'"
		+ $@"WHERE taskid = (SELECT id FROM tasks WHERE uid = '{sanitize(tuid)}' LIMIT 1)
		AND userid = (SELECT id FROM users
			WHERE name = '{sanitize(username)}' AND hash = '{sanitize(hash(token))}')
			AND dateComplete IS NULL;";
		exec(query).Dispose();
	}
	public void Dispose()
	{
		dbcon.Dispose();
	}
}
