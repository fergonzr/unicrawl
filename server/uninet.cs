using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Web;
using System.Net;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

class Server : IDisposable {
	public string prefix  {get;} = "http://*:8080/";
	private Endpoints endpoints;
	private DBQuerier querier;
	private Dictionary<(string, string), MethodInfo> endpointDict;
	// private string responseString = "I like your face\n";
	private HttpListener listener;
	public Server()
	{
		listener = new HttpListener();
		listener.Prefixes.Add(prefix);
		listener.Start();
		querier = new DBQuerier();
		endpoints = new Endpoints(querier);
		PopulateEndpointDict();
		Receive();
	}
	private void PopulateEndpointDict()
	{
		string[] split;
		endpointDict = new Dictionary<(string, string), MethodInfo>();
		foreach (MethodInfo mi in
				typeof(Endpoints).GetMethods()
				.Where(mi => mi.Name.StartsWith("Http_")) )
			endpointDict.Add(
				( (split = mi.Name.Split("_"))[1], "/" + split[2]),
				mi
			);
	}
	public void Receive()
	{
		listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);
	}
	public void Dispose()
	{
		listener.Stop();
	}
	public void Respond(HttpListenerRequest request, HttpListenerResponse response)
	{
		(int status, string responseStr) =
		// check if the endpoint is found
		endpointDict.ContainsKey( (request.HttpMethod, request.Url.AbsolutePath) ) ?
		// If so, invoke it and store the status and the response string
		(ValueTuple<int, string>)
			(endpointDict[ (request.HttpMethod, request.Url.AbsolutePath) ]
			.Invoke(endpoints, new Object[]{HttpUtility.ParseQueryString(request.Url.Query),
				BodyToString(request.InputStream, request.ContentLength64, request.ContentEncoding)}
			))
		// Otherwise, just return 404 not found
		: ( 404, "404 Not found\n");
		
		response.StatusCode = status;
		response.ContentType = "text/plain";
		response.ContentEncoding = new UTF8Encoding();
		WriteToResponse( response.OutputStream, responseStr, response.ContentEncoding );
	}
	private void WriteToResponse(Stream responseStream, string outStr, Encoding e)
	{
		byte[] buffer;
		responseStream.Write(
			buffer = e.GetBytes(outStr),
			0, buffer.Length
		);
		responseStream.Close();
	}
	private string BodyToString(Stream bodyStream, long len, Encoding e)
	{
		byte[] buffer = new byte[len];
		bodyStream.Read(buffer, 0, (int) len);
		return e.GetString(buffer);
	}
	private void ListenerCallback(IAsyncResult result)
	{
		if ( !listener.IsListening )
			return;
		HttpListenerContext context = listener.EndGetContext(result);
		Receive();
		HttpListenerRequest request = context.Request;
		Console.WriteLine($"Obtained new request: {context.Request.Url.AbsolutePath} via {context.Request.HttpMethod}");
		Respond( context.Request, context.Response ); }
}

class Endpoints {
	private DBQuerier querier;
	public Endpoints(DBQuerier querier)
	{
		this.querier = querier;
	}
	public (int, string) Http_GET_users(NameValueCollection query, string body)
		=> (query["project"] != null)
		 ? (200, JsonConvert.SerializeObject(querier.GetUsers(query["project"])))
		 : (400, "{\"error\" : \"Need following parameters: project\"}");
	public (int, string) Http_GET_user(NameValueCollection query, string body)
	{
		User u;
		return (query["name"] != null)
		     ? (
			     ((u = querier.GetUser(query["name"])) != null) ?
		          (200, JsonConvert.SerializeObject(u)) :
		          (404, "{\"error\": \"User not found\"}")
			   )
		     : (400, "{\"error\" : \"Need following parameters: name\"}");
	}
	public (int, string) Http_GET_friends(NameValueCollection query, string body)
	{
		User[] friends;
		return (query["name"] != null)
		     ? (
			     ((friends = querier.GetFriends(query["name"])) != null) ?
		          (200, JsonConvert.SerializeObject(friends)) :
		          (404, "{\"error\": \"User not found\"}")
			   )
		     : (400, "{\"error\" : \"Need following parameters: name\"}");
	}

	public (int, string) Http_GET_tasks(NameValueCollection query, string body)
		=> (200, JsonConvert.SerializeObject(querier.GetAssignments(
			query["user"],
			(query["completed"] != null) ? ( query["completed"] == "true" ) : null,
			query["project"]
		)));
	public (int, string) Http_GET_task(NameValueCollection query, string body)
		=> ((query["uid"] != null)
		 ? (200, JsonConvert.SerializeObject(querier.GetAssignment(query["uid"])))
		 : (400, "{\"error\" : \"Need following parameters: uid\"}"));

	public (int, string) Http_GET_projectInfo(NameValueCollection query, string body){
		try {
		return query["project"] != null
		 ? (200, JsonConvert.SerializeObject(querier.ProjectInfo(query["project"])))
		 : (400, "{\"error\" : \"Need following parameters: project\"}");
		} catch (RecordNotFoundException e) {
			return (404, "{\"error\": \"Project '" + e.val + "' does not exist\"}");
		}
	}

	public (int, string) Http_GET_projects(NameValueCollection query, string body)
		=> query["user"] == null
		?  (200, JsonConvert.SerializeObject(querier.GetProjects()))
		:  (200, JsonConvert.SerializeObject(querier.GetProjects(
			query["user"], query["isSubscribed"] != null)));

	
	public (int, string) Http_GET_characters(NameValueCollection query, string body)
		=> (200, JsonConvert.SerializeObject(querier.GetCharacters()));

	public (int, string) Http_POST_newUser(NameValueCollection query, string body)
	{
		if (string.IsNullOrEmpty(query["name"]) || string.IsNullOrEmpty(query["character"]))
			return (400, "{\"error\": \"Need following parameters: name, character\"}");
		try {
			return (200, JsonConvert.SerializeObject(querier.NewUser(query["name"], query["character"])));
		} catch (NonUniqueFieldException) {
			return (403, "{\"error\" : \"Username '" + query["name"] + "' already taken\"}");
		} catch (RecordNotFoundException) {
			return (404, "{\"error\" : \"Character '" + query["character"] + "' does not exists\"}");
		}
	}

	public (int, string) Http_POST_newCharacter(NameValueCollection query, string body)
	{
		JObject obj;
		try {
			obj = JObject.Parse(body);
		} catch (JsonReaderException) {
			return (400, "{\"error\": \"Unable to parse request\"}");
		}
		if (  string.IsNullOrEmpty( (string) obj["character"])
		   || string.IsNullOrEmpty( (string) obj["asciiArt"]) )
			return (400, "{\"error\": \"Need following parameters: character, asciiArt\"}");
		try {
			querier.NewCharacter((string) obj["character"], (string) obj["asciiArt"]);
		} catch (NonUniqueFieldException e) {
			return (403, "{\"error\": \"Character '" + e.val + "' already exists.\"}");
		}
		return (200, "\"Success\"");
	}

	public (int, string) Http_POST_newProject(NameValueCollection query, string body)
	{
		JObject obj;
		try {
			obj = JObject.Parse(body);
		} catch (JsonReaderException) {
			return (400, "{\"error\": \"Unable to parse request\"}");
		}
		if (  string.IsNullOrEmpty( (string) obj["name"])
		   || string.IsNullOrEmpty( (string) obj["token"])
		   || string.IsNullOrEmpty( (string) obj["projectname"])
		   || string.IsNullOrEmpty( (string) obj["projectopen"]) )
			return (400, "{\"error\": \"Need following parameters: name, token, projectname, projectopen\"}");
		try {
			return (200, JsonConvert.SerializeObject(
				querier.NewProject((string) obj["name"],
				                   (string) obj["token"],
				                   (string) obj["projectname"],
				                   (string) obj["projectopen"]
			)));
		} catch (NonUniqueFieldException e) {
			return (403, "{\"error\": \"Project '" + e.val +"' already exists.\"}");
		} catch (AccessDeniedException) {
			return (403, "{\"error\": \"Access denied\"}");
		}
	}

	public (int, string) Http_POST_newAssignment(NameValueCollection query, string body)
	{
		JObject obj;
		try {
			obj = JObject.Parse(body);
		} catch (JsonReaderException) {
			return (400, "{\"error\": \"Unable to parse request\"}");
		}
		try {
			if (  string.IsNullOrEmpty( (string) obj["title"])
			   || string.IsNullOrEmpty( (string) obj["description"])
			   || string.IsNullOrEmpty( (string) obj["enemy"])
			   || (DateTime) obj["dueDate"] == null
			   || string.IsNullOrEmpty( (string) obj["name"])
			   || string.IsNullOrEmpty( (string) obj["token"])
			   )
				return (400, "{\"error\": \"Need following parameters: title, description, enemy, dueDate, name, token\"}");
		} catch (FormatException) {
				return (400, "{\"error\": \"Invalid date supplied\"}");
		}
		try {
			return (200, JsonConvert.SerializeObject(
				querier.NewAssignment((string) obj["title"],
				                (string) obj["description"],
				                (string) obj["enemy"],
				                ((byte?) obj["rank"] != null) ? (byte) obj["rank"] : (byte) 0,
				                (DateTime) obj["dueDate"],
				                (string) obj["name"],
				                (string) obj["token"]
			)));
		} catch (RecordNotFoundException e) {
			return (404, "{\"error\": \"Character '" + e.val + "' does not exist\"}");
		} catch (AccessDeniedException) {
			return (403, "{\"error\": \"Access denied\"}");
		} catch (MakeYourOwnProjectFirstException e) {
			return (403, "{\"error\": \"User '" + e.val + "' is not master of any project\"}");
		}
	}
	public (int, string) Http_POST_subscribeProj(NameValueCollection query, string body)
	{
		JObject obj;
		try {
			obj = JObject.Parse(body);
		} catch (JsonReaderException) {
			return (400, "{\"error\": \"Unable to parse request\"}");
		}
		if (  string.IsNullOrEmpty( (string) obj["name"])
		   || string.IsNullOrEmpty( (string) obj["token"])
		   || string.IsNullOrEmpty( (string) obj["projectname"]) )
			return (400, "{\"error\": \"Need following parameters: name, token, projectname\"}");
		try {
			querier.SubscribeProj(
				(string) obj["name"],
				(string) obj["token"],
				(string) obj["projectname"]
			);
		} catch (RecordNotFoundException e) {
			return (404, "{\"error\": \"Project '" + e.val + "' does not exist\"}");
		} catch (AccessDeniedException) {
			return (403, "{\"error\": \"Access denied\"}");
		}
		return (200, "\"Success\"");
	}
	public (int, string) Http_POST_completeAssignment(NameValueCollection query, string body)
	{
		JObject obj;
		try {
			obj = JObject.Parse(body);
		} catch (JsonReaderException) {
			return (400, "{\"error\": \"Unable to parse request\"}");
		}
		if (  string.IsNullOrEmpty( (string) obj["name"])
		   || string.IsNullOrEmpty( (string) obj["token"])
		   || string.IsNullOrEmpty( (string) obj["uid"]) )
			return (400, "{\"error\": \"Need following parameters: name, token, uid\"}");
		try {
			querier.CompleteAssignment(
				(string) obj["name"],
				(string) obj["token"],
				(string) obj["uid"]
			);
		} catch (RecordNotFoundException e) {
			return (404, "{\"error\": \"Assignment with uid '" + e.val + "' is not assigned.\"}");
		} catch (AccessDeniedException) {
			return (403, "{\"error\": \"Access denied\"}");
		}
		return (200, "\"Success\"");
	}
}

class Program {
	public static void Main()
	{
		using Server s = new Server();
		while (true)
			if ( Console.ReadLine().ToLower() == "q" )
				return;
	}
}
