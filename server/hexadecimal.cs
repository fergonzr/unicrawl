/*
 * A class to implement byte[] conversion to and from hexadecimal
 * strings.
 */
using System;
using System.Linq;

class Hex {
	// TO
	// single
	static private char hexChar(int n)
	{
		return (n < 10)
			? (char) ('0' + n)
			: (char) ('a' + n - 10);
	}
	// double
	static private string ToHex(byte b)
	{
		return hexChar(b / 16).ToString() + hexChar(b % 16).ToString();
	}
	static public string Bytes2Hex(byte[] bytes)
	{
		return string.Join("", bytes.Select(b => ToHex(b)));
	}
	// FROM
	// single
	static private byte decodeHexSingle(char c)
	{
		if ( c >= '0' && c <= '9' )
			return (byte) (c - '0');
		if ( c >= 'a' && c <= 'f' )
			return (byte) (c - 'a' + 10);
		return 0;
	}
	// double
	static private byte Hex2Byte(string s)
	{
		return (byte) (decodeHexSingle(s[0]) * 16 + decodeHexSingle(s[1]));
	}
	// multiple, recursive ;)
	static public byte[] Hex2Bytes(string hexstr)
	{
		if (hexstr.Length == 2)
			return new byte []{Hex2Byte(hexstr)};
		if (hexstr.Length == 1)
			return new byte []{decodeHexSingle(hexstr[0])};
		byte[] later = Hex2Bytes(hexstr[2..]);
		byte[] rel = new byte[later.Length + 1];
		rel[0] = Hex2Byte(hexstr[0..2]);
		later.CopyTo(rel, 1);
		return rel;
	}
}
